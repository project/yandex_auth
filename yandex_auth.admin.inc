<?php

function yandex_auth_settings_form() {
  $form = array();

  $form['yandex_auth_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Link text for Yandex mail'),
    '#default_value' => variable_get('yandex_auth_link', 'Go to Yandex.Mail'),
    '#required' => TRUE
  );


  return system_settings_form($form);
}